package data;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Defines table and column names for the movie database
 * Also declare and define Content Authority, Content URI, Content Type,
 */
public class MovieContract {

    public static final String LOG_TAG = MovieContract.class.getSimpleName();

    // 1. Content Authority is the name of the content Provider
    // Conventionally we use the package name of the app as the content authority
    public static final String CONTENT_AUTHORITY = "moviegenie.an.moviegenie";
    // 2. Use CONTENT_AUTHORITY to create the base of all URI's which apps will use to contact
    // the content provider.
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    // 3.Possible paths (appended to base content URI for possible URI's)
    // For instance, content://moviegenie.an.moviegenie/movie/ is a valid path for
    // looking at movie data.

    public static final String PATH_MOVIE = "movie";
    public static final String PATH_TRAILER = "videos";
    public static final String PATH_REVIEW = "reviews";
    public static final String PATH_FAVORTIES = "favorites";


    /* 4. Inner class that defines the table contents of the movie table */
    public static final class MovieEntry implements BaseColumns {

        // Content Uri for movie table
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_MOVIE).build();

        // 5. Content Types
        // CURSOR_DIR_BASE_TYPE: access multiple elements of the same type
        // You use directory-based URIs to access multiple elements of the same type (eg. all movies
        // of a specific genre).All CRUD-operations are possible with directory-based URIs.
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MOVIE;
        // CURSOR_ITEM_BASE_TYPE:
        // You use id-based URIs if you want to access a specific element.
        // You cannot create objects using an id-based URI – but reading, updating and deleting is possible.
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MOVIE;

        //table name
        public static final String TABLE_NAME = "movie";

        // Columns within the table
        public static final String COLUMN_QRY_CATEGORY = "query_category";
        // movie id as returned by API
        public static final String COLUMN_MOVIE_ID = "movie_id";
        // URL in .jpg for the poster to display(stored as strings)
        public static final String COLUMN_POSTER_URL = "posterUrl";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_BACKDROP = "backdrop";

        // Date, stored as string - 2016-08-03
        public static final String COLUMN_RELEASE_DATE = "date";

        // Short description and long description of the movie, as provided by API.
        public static final String COLUMN_OVERVIEW = "overview";

        // voteAverage is stored as a double representing percentage
        public static final String COLUMN_VOTE_AVERAGE = "voteAverage";


        // 6. Append the id to the base Content_URI
        //content://moviegenie.an.moviegenie/movie/id
        public static Uri buildMovieUri(long id) {
            // Use the ContentUris method to produce the base URI for the movie
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        // Uri with sortOrderCategory: popular
        //content://moviegenie.an.moviegenie/movie/popular
        public static Uri buildMovieUriWithQueryCriteria(String queryCategory) {
            return CONTENT_URI.buildUpon().appendPath(queryCategory).build();
        }

        /** content://moviegenie.an.moviegenie/movie/popular
         * getPathSegements will return a list, get(0) returns movie, get(1) returns popular
         *
         * api.themoviedb.org/3/movie/popular?
         * returns popular
         */
         public static String getQueryStringFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
         }

        // getMoveIdFromUri
        //content://moviegenie.an.moviegenie/movie/movieId
        public static String getMovieIdFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }


    }// MovieEntry

    // Trailers
    public static final class TrailerEntry implements BaseColumns{
        // Content Uri for trailer table
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_TRAILER).build();
        // only one cursor type to retrieve
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_TRAILER;
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_TRAILER;


        public static final String COLUMN_TRAILER_KEY = "trailer_key";
        public static final String COLUMN_TRAILER_TITLE = "trailer_title";
        // col with movie id as the foreign key
        public static final String COLUMN_REMOTE_MOVIE_ID = "remote_movie_id";
        public static final String COLUMN_TYPE = "type";

        // Table name
        public static final String TABLE_NAME = "trailer";

        // This will return all the trailers for a particular movie that you would return
        // as a cursor by trailerloader to recyclerview.
        //content://moviegenie.an.moviegenie/trailer/movieId
        public static Uri buildTrailerUri(long id){
            // Use the ContentUris method to produce the base URI for the movie
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        // content://moviegenie.an.moviegenie/trailer/trailerId
        public static String getQueryStringFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }

    }


    // reviews table
    public static final class ReviewsEntry implements BaseColumns{
        // Content Uri for review table
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_REVIEW).build();
        // only one cursor type to retrieve
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_REVIEW;
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_REVIEW;

        // Table name
        public static final String TABLE_NAME = "reviews";

        // columns
        public static final String COLUMN_DESC = "description";
        // col with movie id as the foreign key
        public static final String COLUMN_REMOTE_MOVIE_ID = "remote_movie_id";
        public static final String COLUMN_REVIEWER = "reviewer";
        public static final String COLUMN_TYPE = "type";

        // This is used when inserting into the database
        //content://moviegenie.an.moviegenie/reviews/reviewid
        public static Uri buildReviewsUri(long reviewid){
            // Use the ContentUris method to produce the base URI for the movie
            return ContentUris.withAppendedId(CONTENT_URI, reviewid);
           // ContentUris.withAppendedId(CONTENT_URI, id);
        }
        // content://moviegenie.an.moviegenie/reviews/reviewsId
        public static String getQueryStringFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }

    public static final class FavoritesEntry implements BaseColumns{

        // Content Uri for favorites table
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_FAVORTIES).build();

        //COntent types:
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_FAVORTIES;
        // for specific element
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MOVIE;

        // Storing all the columns as movie table because you want to show the favorites
        // on click from settings. It's easier to keep all the data to avoid multiple queries
        //table name
        public static final String TABLE_NAME = "favorites";

        // Columns within the table
        public static final String COLUMN_QRY_CATEGORY = "query_category";
        // movie id as returned by API
        public static final String COLUMN_MOVIE_ID = "movie_id";
        // URL in .jpg for the poster to display(stored as strings)
        public static final String COLUMN_POSTER_URL = "posterUrl";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_BACKDROP = "backdrop";

        // Date, stored as string - 2016-08-03
        public static final String COLUMN_RELEASE_DATE = "date";

        // Short description and long description of the movie, as provided by API.
        public static final String COLUMN_OVERVIEW = "overview";

        // voteAverage is stored as a double representing percentage
        public static final String COLUMN_VOTE_AVERAGE = "voteAverage";


        //query for all favorites - build uri
        //  content://moviegenie.an.moviegenie/favorites/favorites
        public static Uri buildFavoritesUriWithQueryCriteria(String queryCategory) {
            return CONTENT_URI.buildUpon().appendPath(queryCategory).build();
        }

        // This is used to insert into db  and retrieve from db (for deletion)
        // content://moviegenie.an.moviegenie/favorites/favoritesId
        public static Uri buildFavoritesUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
        // content://moviegenie.an.moviegenie/favorites/favoritesId or favorites
        public static String getQueryStringFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }



}