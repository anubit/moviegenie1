package data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Content Provider class. You build the URI matcher here
 *
 * Define constants for each URI.
 * Build the uriMatcher method(uniform resource identifier in Contract class)
 * Build the queryBuilder (useful if you use multiple tables, use join)
 * implement all the methods of content provider: query
 *
 */
public class MovieProvider extends ContentProvider {

    // We need constants for each type of URi
    // build a tree of UriMatcher objects

    private static final String LOG_TAG = MovieProvider.class.getSimpleName();
    private static final int MOVIE = 100;
    private static final int MOVIE_POPULAR = 101;
    private static final int MOVIE_TOP_RATED = 102;
    private static final int MOVIE_WITH_ID = 103;
    public static final int TRAILERS_WITH_MOVIE_ID = 104;
    private static final int TRAILERS = 105;

    private static final int REVIEWS = 106;
    public static final int REVIEWS_WITH_MOVIE_ID = 107;
    // No api request for favorites
    public static final int FAVORITES = 108;
    public static final int FAVORITES_AS_CATEGORY = 109;
    public static final int FAVORITES_WITH_MOVIE_ID = 110;

    public static final String FAVORITE_CATEGORY = "favorites";

    // build the uri matcher
    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private MovieDbHelper mMovieDbHelper;

    // THis variable helps to build the SQLITEQuery - we are using inner join to join
    // 2 tables.
    private static final SQLiteQueryBuilder
            sqlLiteQueryBuilderMovie, sqlLiteQueryBuilderTrailer,sqlLiteQueryBuilderReviews,
            sqlLiteQueryBuilderFavorites;



    /**
     * buildUriMatcher()
     * This UriMatcher will match each URI to the MOVIES, MOVIES_BY_POPULAR, MOVIES_BY_TOPRATED,
     * Trailers, reviews, integer constants defined above.  You can test this by uncommenting the
     * testUriMatcher test within TestUriMatcher.
     **/
    static UriMatcher buildUriMatcher() {
        // 1) The code passed into the constructor represents the code to return for the root
        // URI.  It's common to use NO_MATCH as the code for this case. Add the constructor below.
        // You are starting the Uri to match anything to start with no match
        UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        String authority = MovieContract.CONTENT_AUTHORITY;

        // 2) Use the addURI function to match each of the types.  Use the constants from
        // MovieContract to help define the types to the UriMatcher.
        // Syntax: addURI(contentAuthority, String path, int code);
        // you are matching location (*) can be any thing and # for date
        sUriMatcher.addURI(authority, MovieContract.PATH_MOVIE, MOVIE);
        sUriMatcher.addURI(authority, MovieContract.PATH_MOVIE+"/#",MOVIE_WITH_ID); ;
        sUriMatcher.addURI(authority, MovieContract.PATH_MOVIE+"/popular", MOVIE_POPULAR);
        sUriMatcher.addURI(authority, MovieContract.PATH_MOVIE+"/top_rated", MOVIE_TOP_RATED);
        sUriMatcher.addURI(authority, MovieContract.PATH_TRAILER+"/#", TRAILERS_WITH_MOVIE_ID);
        sUriMatcher.addURI(authority, MovieContract.PATH_TRAILER, TRAILERS);
        sUriMatcher.addURI(authority, MovieContract.PATH_REVIEW, REVIEWS);
        sUriMatcher.addURI(authority, MovieContract.PATH_REVIEW+"/#",REVIEWS_WITH_MOVIE_ID);
        sUriMatcher.addURI(authority, MovieContract.PATH_FAVORTIES, FAVORITES);
        sUriMatcher.addURI(authority, MovieContract.PATH_FAVORTIES+"/#",FAVORITES_WITH_MOVIE_ID);
        sUriMatcher.addURI(authority, MovieContract.PATH_FAVORTIES+"/favorites", FAVORITES_AS_CATEGORY);


        return sUriMatcher;
    }

    /**
     * This method helps you to build queries to send it to SQLiteDatabase
     * can use setTables method to inner join multiple tables
     */
    static {
        sqlLiteQueryBuilderMovie = new SQLiteQueryBuilder();
        sqlLiteQueryBuilderMovie.setTables(
                MovieContract.MovieEntry.TABLE_NAME );

        sqlLiteQueryBuilderTrailer = new SQLiteQueryBuilder();
        sqlLiteQueryBuilderTrailer.setTables(MovieContract.TrailerEntry.TABLE_NAME);

        sqlLiteQueryBuilderReviews = new SQLiteQueryBuilder();
        sqlLiteQueryBuilderReviews.setTables(MovieContract.ReviewsEntry.TABLE_NAME);

        sqlLiteQueryBuilderFavorites = new SQLiteQueryBuilder();
        sqlLiteQueryBuilderFavorites.setTables(MovieContract.FavoritesEntry.TABLE_NAME);

    };

    /**
     * Implement methods of Content Provider
     * @return
     */
    @Override
    public boolean onCreate(){
        mMovieDbHelper = new MovieDbHelper(getContext());
        //Log.d(LOG_TAG,"dbhelper instance : " + mMovieDbHelper.toString());
        return true;
    }


    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {

        Log.d(LOG_TAG,"inside query of content provider");
        Log.d(LOG_TAG,"Uri in query, look for match : " + uri.toString());

        Cursor retCursor;
        // To match the uri, you call via sUriMatcher.match(uri)
        Log.d(LOG_TAG,"sUriMatcher " + sUriMatcher.match(uri));

        switch(sUriMatcher.match(uri)){
            //content://moviegenie.an.moviegenie/movie/popular
            case MOVIE_POPULAR:
            case MOVIE_TOP_RATED:
            {
                Log.d(LOG_TAG,"matched -MOVIE BY TOPRATED/POPULAR :" );
                retCursor =  getMovieByQueryCategory(uri,projection,sortOrder);
               // Log.d(LOG_TAG,"retCursor = " + retCursor.toString());
                break;
            }
            //content://moviegenie.an.moviegenie/videos/movieid
            case TRAILERS_WITH_MOVIE_ID:
            {
                Log.d(LOG_TAG, "matched - Trailers with movie id uri in query matching method =" + uri);
                retCursor = getTrailersByMovieId(uri,projection,sortOrder);
               // Log.d(LOG_TAG,"retCursor = " + retCursor.toString());
                break;
            }
            //content://moviegenie.an.moviegenie/movie/
            case MOVIE: {
             //   Log.d(LOG_TAG, "matched - MOVIE BY movie");
                retCursor = getContext().getContentResolver().
                        query(uri, projection, null, null, null);
              //  Log.d(LOG_TAG,"retCursor = " + retCursor.toString());
                break;

            }
            //content://moviegenie.an.moviegenie/movie/videos/
            case TRAILERS: {
                Log.d(LOG_TAG, "matched - Trailers");
                retCursor = getContext().getContentResolver().
                        query(uri, projection, null, null, null);
              //  Log.d(LOG_TAG,"retCursor = " + retCursor.toString());
                break;

            }

            //movie/#
            case MOVIE_WITH_ID: {
              //  Log.d(LOG_TAG," matched - get the movie by id =" + MOVIE_WITH_ID);
                retCursor = getMovieById(uri,projection,null);
               // Log.d(LOG_TAG,"retCursor = " + retCursor.toString());
                break;
            }

            case REVIEWS:{
                Log.d(LOG_TAG, "matched - Reviews");
                retCursor = getContext().getContentResolver().
                        query(uri, projection, null, null, null);
                Log.d(LOG_TAG,"retCursor = " + retCursor.toString());
                break;
            }

            case REVIEWS_WITH_MOVIE_ID:
            {
                Log.d(LOG_TAG, "matched - Trailers with movie id uri in query matching method =" + uri);
                retCursor = getReviewsByMovieId(uri,projection,sortOrder);
              //  Log.d(LOG_TAG,"retCursor = " + retCursor.toString());
                break;
            }

            case FAVORITES_WITH_MOVIE_ID:
            {
                Log.d(LOG_TAG, "matched favorites with movie id =" + uri);
                retCursor = getFavoritesByMovieId(uri,projection,null);
                break;
            }
            case FAVORITES_AS_CATEGORY:
            {
                Log.d(LOG_TAG, "matched favorites as category - favorites uri =" + uri);
                retCursor = getFavoritesByQueryCategory(uri,projection,null);
                break;
            }
            case FAVORITES:
            {
                retCursor = getContext().getContentResolver().
                        query(uri, projection, null, null, null);
                break;
            }


            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }

    /**
     * Get Query by MovieId: content://<content_authority>/id
     * This method takes in uri, projection, sort order and returns cursor
     * This method internally builds the selection and the selection args for the table: movie
     * https://api.themoviedb.org/3/movie/{movie_id}?api_key
     */
    // selection is referred by "?" selection will be popular or top rated
      private static final String sQueryParamSelection =
          MovieContract.MovieEntry.COLUMN_QRY_CATEGORY + " = ? ";
    //  Log.d(LOG_TAG,"inside query of content provider");
      private static final String sQueryParamIdSelection =
              MovieContract.MovieEntry.COLUMN_MOVIE_ID + "= ?";

    private Cursor getMovieById(Uri uri, String[] projection,String sortOrder){

        Log.d(LOG_TAG,"inside getMovieById in MovieProvider");
        Log.d(LOG_TAG,"the uri to query from database " + uri.toString());
        //get the query category from the uri : returns popular or top_rated
        String queryParam = MovieContract.MovieEntry.getQueryStringFromUri(uri);
        Log.d(LOG_TAG,"queryParam " + queryParam);
        String[] selectionArgs;
        String selection;

        selection =  sQueryParamIdSelection;
        Log.d(LOG_TAG,"selection " + selection);
        selectionArgs = new String[]{ queryParam };
        Log.d(LOG_TAG,"selection args = " + selectionArgs[0]);
        Log.d(LOG_TAG," project cols:" + projection[1].toString() + " " + projection[2].toString() + " "
        + projection[3].toString());
        Cursor retCursor = sqlLiteQueryBuilderMovie.query(mMovieDbHelper.getReadableDatabase(),
                projection,selection,selectionArgs,null,null,null);
        Log.d(LOG_TAG,"ret cursor data to pass to query " + retCursor.getCount());

        return retCursor;
    }

    /**
     * Get query category : popular or toprated
     * This method takes in uri, projection, sort order and returns cursor - with trailer title,
     * This method internally builds the selection and the selection args for the table: movie
     */
    private Cursor getMovieByQueryCategory(Uri uri, String[] projection,String sortOrder){

        Log.d(LOG_TAG,"called from getMovieByQueryCategory in MovieProvider");
      //  Log.d(LOG_TAG,"the uri " + uri.toString());
        //get the query param: here it will return the id :
        String queryParam = MovieContract.MovieEntry.getQueryStringFromUri(uri);
        Log.d(LOG_TAG,"queryParam  " + queryParam);
        String[] selectionArgs;
        String selection;

        // where movie.movieid is null or trailer.movieid is null
        selection = sQueryParamSelection;
        Log.d(LOG_TAG,"selection " + selection);
        selectionArgs = new String[]{queryParam} ;

        Cursor retCursor;
        retCursor = sqlLiteQueryBuilderMovie.query(mMovieDbHelper.getReadableDatabase(),
                    projection,selection,selectionArgs,null,null,null);


        return retCursor;
    }

    /**
     *  this function retrieves all favorites movies - used in adapter to show favorites
     */
    private Cursor getFavoritesByQueryCategory(Uri uri, String[] projection,String sortOrder){

        Log.d(LOG_TAG,"inside getFavoriteByQueryCategory");

        String queryParam = MovieContract.FavoritesEntry.getQueryStringFromUri(uri);
        Log.d(LOG_TAG,"queryParam  " + queryParam);
        String[] selectionArgs;
        String selection;

        // where movie.movieid is null or trailer.movieid is null
        selection = sQueryParamSelection;
        Log.d(LOG_TAG,"selection " + selection);
        selectionArgs = new String[]{queryParam} ;

        Cursor retCursor = sqlLiteQueryBuilderFavorites.query(mMovieDbHelper.getReadableDatabase(),
                projection,selection,selectionArgs,null,null,null);

        return retCursor;
    }


    /**
     * get trailers by movie id
     *
     */
    // selection is referred by "?" selection will be movie id
    private static final String sTrailerQueryParamSelection =
            MovieContract.TrailerEntry.COLUMN_REMOTE_MOVIE_ID + " = ? ";
    public Cursor getTrailersByMovieId(Uri uri, String[] projection,String sortOrder) {
        Log.d(LOG_TAG,"the uri " + uri.toString());
        // get the movie id.
        String queryParam = MovieContract.TrailerEntry.getQueryStringFromUri(uri);
        Log.d(LOG_TAG,"queryParam  " + queryParam);
        String[] selectionArgs;
        String selection;

        selection = sTrailerQueryParamSelection;
        Log.d(LOG_TAG,"selection " + selection);
        selectionArgs = new String[]{queryParam};
        Log.d(LOG_TAG,"selection args = " + selectionArgs[0]);
        Log.d(LOG_TAG," project cols:" + projection[1].toString() + " " + projection[2].toString() + " "
                + projection[3].toString());
        Cursor retCursor = sqlLiteQueryBuilderTrailer.query(mMovieDbHelper.getReadableDatabase(),
                projection,selection,selectionArgs,null,null,null);
        Log.d(LOG_TAG,"ret cursor data to pass to query " + retCursor.getCount());

        return retCursor;

    }

    // selection is referred by "?" selection will be movie id
    private static final String sReviewsQueryParamSelection =
            MovieContract.ReviewsEntry.COLUMN_REMOTE_MOVIE_ID + " = ? ";
    public Cursor getReviewsByMovieId(Uri uri, String[] projection,String sortOrder) {
        Log.d(LOG_TAG,"the uri " + uri.toString());
        // get the movie id.
        String queryParam = MovieContract.ReviewsEntry.getQueryStringFromUri(uri);
        Log.d(LOG_TAG,"queryParam  " + queryParam);
        String[] selectionArgs;
        String selection;

        selection = sReviewsQueryParamSelection;
        Log.d(LOG_TAG,"selection " + selection);
        selectionArgs = new String[]{queryParam};
        Log.d(LOG_TAG,"selection args = " + selectionArgs[0]);
        Log.d(LOG_TAG," project cols:" + projection[1].toString() + " " + projection[2].toString() + " "
                );
        Cursor retCursor = sqlLiteQueryBuilderReviews.query(mMovieDbHelper.getReadableDatabase(),
                projection,selection,selectionArgs,null,null,null);
        Log.d(LOG_TAG,"ret cursor data to pass to query " + retCursor.getCount());

        return retCursor;

    }

    private static final String sFavoriteQueryParamSelection =
            MovieContract.FavoritesEntry.COLUMN_MOVIE_ID + " = ? ";
    public Cursor getFavoritesByMovieId(Uri uri, String[] projection,String sortOrder) {
        Log.d(LOG_TAG,"inside getFavorites by movie id");
        Log.d(LOG_TAG,"the uri " + uri.toString());
        // get the movie id.
        String queryParam = MovieContract.FavoritesEntry.getQueryStringFromUri(uri);
        Log.d(LOG_TAG,"queryParam  " + queryParam);
        String[] selectionArgs;
        String selection;

        selection = sFavoriteQueryParamSelection;
        Log.d(LOG_TAG,"selection " + selection);
        selectionArgs = new String[]{queryParam};
        Log.d(LOG_TAG,"selection args = " + selectionArgs[0]);
        Log.d(LOG_TAG," project cols:" + projection[1].toString() + " " + projection[2].toString() );
        Cursor retCursor = sqlLiteQueryBuilderFavorites.query(mMovieDbHelper.getReadableDatabase(),
                projection,selection,selectionArgs,null,null,null);
        Log.d(LOG_TAG,"ret cursor data to pass to query " + retCursor.getCount());

        return retCursor;

    }
    /**
     * This method matches the Uri code and returns type of the item type
     * based on the code. Returns CONTENT_TYPE.
     * @param uri
     * @return
     */

    @Nullable
    @Override
    public String getType(Uri uri) {
        // Use the Uri Matcher to determine what kind of URI this is.
        Log.d(LOG_TAG," getType: to match Ur " + uri.toString());
        final int match = sUriMatcher.match(uri);

        switch (match) {

            case MOVIE_POPULAR:
                return MovieContract.MovieEntry.CONTENT_TYPE;
            case MOVIE_TOP_RATED:
                return MovieContract.MovieEntry.CONTENT_TYPE;
            case MOVIE_WITH_ID:
                return MovieContract.MovieEntry.CONTENT_ITEM_TYPE;
            case MOVIE:
                return MovieContract.MovieEntry.CONTENT_TYPE;
            case TRAILERS_WITH_MOVIE_ID:
                return MovieContract.TrailerEntry.CONTENT_TYPE;
            case REVIEWS:
                return MovieContract.ReviewsEntry.CONTENT_TYPE;
            case FAVORITES:
                return MovieContract.MovieEntry.CONTENT_TYPE;
            case TRAILERS:
                return MovieContract.TrailerEntry.CONTENT_TYPE;
            case FAVORITES_WITH_MOVIE_ID:
                return  MovieContract.FavoritesEntry.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    /**
     * You are inserting the data into the database
     * You require only the base uri.
     *  When you modify for the database, you need to notify the content observer to notify
     * the change. Cursors are registered as notify to all its descendants.
     * That means if you notify the root URi then all its descendants of the URI are also
     * notified.
     // Just like content resolver used for content provider, we can use content resolver
     // to notify the content observer.
     // Content observer returns an uri based on the Id which is different from the root URI (without
     // the id). You have to return the URI, we can use the buildWeatherUri method to build
     // the uri to return with the id.
     // NOTE: IMPORTANT, you are inserting into the database _id. _id is a column needed to create
     // the database.
     * @param uri
     * @param contentValues
     * @return
     */
    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        Log.d(LOG_TAG,"Insert method: Uri " + uri);
        //1. get hold of database to write to
        final SQLiteDatabase db = mMovieDbHelper.getWritableDatabase();
        // 2. match the uri to figure out which table to insert into
        final int match = sUriMatcher.match(uri);
        Uri returnUri;
        long _id = 0;

        switch (match) {
            case MOVIE:
            {
                Log.d(LOG_TAG,"insert into the database for sort order " );
                _id = db.insert(MovieContract.MovieEntry.TABLE_NAME, null, contentValues);
                if (_id > 0) {
                    returnUri = MovieContract.MovieEntry.buildMovieUri(_id);
                    Log.d(LOG_TAG,"inserted new row into db, its Id: " + returnUri);
                }
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }

            case TRAILERS:
            {
                Log.d(LOG_TAG,"insert into the database for trailers " + TRAILERS);
                _id = db.insert(MovieContract.TrailerEntry.TABLE_NAME, null, contentValues);
                if (_id > 0) {
                    returnUri = MovieContract.TrailerEntry.buildTrailerUri(_id);
                    Log.d(LOG_TAG,"inserted new row into db, its Id: " + returnUri);
                }
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case REVIEWS: {
                Log.d(LOG_TAG,"insert into the database for review " + REVIEWS);
                _id = db.insert(MovieContract.ReviewsEntry.TABLE_NAME, null, contentValues);
                if (_id > 0) {
                    returnUri = MovieContract.ReviewsEntry.buildReviewsUri(_id);
                    Log.d(LOG_TAG,"inserted new row into db, its Id: " + returnUri);
                }
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;

            }
            case FAVORITES_WITH_MOVIE_ID: {
                Log.d(LOG_TAG, "Insert: operation - favorite with Movie id");
                _id = db.insert(MovieContract.FavoritesEntry.TABLE_NAME,null,contentValues);
                if (_id > 0)
                    returnUri = MovieContract.FavoritesEntry.buildFavoritesUri(_id);
                else
                    throw new android.database.SQLException(("failed to insert row into ") + uri);
                break;
            }

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        // you are using the contentResolver to notify the contentObserver to notify the
        // changes to DB
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    //Delete records

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {

        Log.d(LOG_TAG, " inside delete of provider");
        SQLiteDatabase db = mMovieDbHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri;
        int rowsDeleted = 0;

        if (null == selection) selection = "1";
        switch (match){
            case FAVORITES: {
                Log.d(LOG_TAG," favorites selected to delete operation");
                rowsDeleted = db.delete(MovieContract.FavoritesEntry.TABLE_NAME, selection, selectionArgs);
                break;
            }
            case FAVORITES_WITH_MOVIE_ID: {
                Log.d(LOG_TAG," favorites selected to delete operation");
                rowsDeleted = db.delete(MovieContract.FavoritesEntry.TABLE_NAME, selection, selectionArgs);
                break;
            }

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);

        }
        //  A null value deletes all rows.  Notify the uri listeners (using the content resolver)
        // if the rowsDeleted != 0 or the selection is null.
        // Oh, and you should notify the listeners here.
        // that means that the rows are deleted.
        if (rowsDeleted != 0) {
            // you are using the contentResolver to notify the contentObserver to notify the
            // changes to DB if rowsDeleted != 0
            getContext().getContentResolver().notifyChange(uri, null);

        }

        return rowsDeleted;
    }

    @Override
    public int update(
            Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = mMovieDbHelper.getWritableDatabase();

        final int match = sUriMatcher.match(uri);
        Uri returnUri;
        int rowsUpdated = 0;
        // Student: This is a lot like the delete function.  We return the number of rows impacted
        // by the update.
        if (null == selection) selection = "1";

        Log.d(LOG_TAG," update function in query ");
        switch (match){
            case MOVIE:
            {
                Log.d(LOG_TAG, "rows updated with sort order " );
                rowsUpdated = db.update(MovieContract.MovieEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            }
            case TRAILERS:
            {
                Log.d(LOG_TAG, "rows updated for trailers " );
                rowsUpdated = db.update(MovieContract.TrailerEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            }
            case FAVORITES:
            {
                rowsUpdated = db.update(MovieContract.MovieEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);

        }

        // You have updated the rows at this point if != 0
        if (rowsUpdated != 0) {

            // you are using the contentResolver to notify the contentObserver to notify the
            // changes to DB if rowsDeleted != 0
            getContext().getContentResolver().notifyChange(uri, null);

        }
        return rowsUpdated;
    }

    // Doing bulk inserts

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        Log.d(LOG_TAG,"bulk insert " + uri.toString());
        final SQLiteDatabase db = mMovieDbHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Log.d(LOG_TAG,"match = " + match);
        int returnCount;
        switch (match) {
            case MOVIE:
                returnCount = 0;
                db.beginTransaction();
                try {
                    for (ContentValues value : values) {
                        //normalizeDate(value);
                       // long _id = db.insert(MovieContract.MovieEntry.TABLE_NAME, null, value);
                        Uri returnUri = insert(uri,value);
                        if (returnUri != null) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            case TRAILERS:
                returnCount = 0;
                db.beginTransaction();
                try {
                    for (ContentValues value : values) {
                        Uri returnUri = insert(uri,value);
                        if (returnUri != null) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            case REVIEWS:
                returnCount = 0;
                db.beginTransaction();
                try {
                    for (ContentValues value : values) {

                        Uri returnUri = insert(uri,value);
                        if (returnUri != null) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;


            default:
                return super.bulkInsert(uri, values);
        }
    }
}
