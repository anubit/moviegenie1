package data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * This class creates a movie DB - manages the local database for movie data
 */
public class MovieDbHelper  extends SQLiteOpenHelper{

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "movie.db";

    public MovieDbHelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    // Create movie table
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        final String SQL_CREATE_MOVIE_TABLE = "CREATE TABLE " + MovieContract.MovieEntry.TABLE_NAME +
                "  ( " +

                MovieContract.MovieEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
                // the ID of the location entry associated with this weather data
                MovieContract.MovieEntry.COLUMN_MOVIE_ID + " INTEGER NOT NULL , " +

                MovieContract.MovieEntry.COLUMN_POSTER_URL + " TEXT NOT NULL, " +
                MovieContract.MovieEntry.COLUMN_TITLE + " TEXT NOT NULL, " +
                MovieContract.MovieEntry.COLUMN_RELEASE_DATE + " TEXT NOT NULL," +
                MovieContract.MovieEntry.COLUMN_VOTE_AVERAGE + " REAL NOT NULL, " +
                MovieContract.MovieEntry.COLUMN_OVERVIEW + " REAL NOT NULL, " +
                MovieContract.MovieEntry.COLUMN_BACKDROP + " TEXT NOT NULL, " +
                MovieContract.MovieEntry.COLUMN_QRY_CATEGORY + " TEXT NOT NULL, " +


                "UNIQUE (" + MovieContract.MovieEntry.COLUMN_TITLE + ") ON CONFLICT REPLACE);";

               // + ");";

        Log.d("MovieDbHelper.java", "create table syntax " + SQL_CREATE_MOVIE_TABLE);

                // per location, it's created a UNIQUE constraint with REPLACE strategy
        //sqLiteDatabase.execSQL(SQL_CREATE_MOVIE_TABLE);

        final String SQL_CREATE_TRAILER_TABLE = "CREATE TABLE " + MovieContract.TrailerEntry.TABLE_NAME +
                "  ( " +

                MovieContract.TrailerEntry._ID + " INTEGER PRIMARY KEY, " +

                MovieContract.TrailerEntry.COLUMN_REMOTE_MOVIE_ID + " INTEGER NOT NULL, " +
                // the ID of the location entry associated with this weather data
                MovieContract.TrailerEntry.COLUMN_TRAILER_TITLE + " TEXT NOT NULL , " +
                MovieContract.TrailerEntry.COLUMN_TRAILER_KEY + " TEXT NOT NULL, " +
                MovieContract.TrailerEntry.COLUMN_TYPE + " TEXT NOT NULL, " +

                "UNIQUE (" + MovieContract.TrailerEntry.COLUMN_TRAILER_KEY + ") ON CONFLICT REPLACE " +
                // Set up the movie _id column as a foreign key to trailer table.
                " FOREIGN KEY (" + MovieContract.TrailerEntry.COLUMN_REMOTE_MOVIE_ID + ") REFERENCES " +
                MovieContract.MovieEntry.TABLE_NAME + " (" + MovieContract.MovieEntry.COLUMN_MOVIE_ID + ") " +
                ");";

        Log.d("MovieDbHelper.java" , "create trailer table syntax: " + SQL_CREATE_TRAILER_TABLE);

        final String SQL_CREATE_REVIEW_TABLE = "CREATE TABLE " + MovieContract.ReviewsEntry.TABLE_NAME +
                "  ( " +

                MovieContract.ReviewsEntry._ID + " INTEGER PRIMARY KEY, " +
                MovieContract.ReviewsEntry.COLUMN_REMOTE_MOVIE_ID + " INTEGER NOT NULL, " +
                // the ID of the location entry associated with this weather data
                MovieContract.ReviewsEntry.COLUMN_REVIEWER + " TEXT NOT NULL , " +
                MovieContract.ReviewsEntry.COLUMN_DESC + " TEXT NOT NULL , " +
                MovieContract.TrailerEntry.COLUMN_TYPE + " TEXT NOT NULL, " +

                "UNIQUE (" + MovieContract.ReviewsEntry.COLUMN_REVIEWER + ") ON CONFLICT REPLACE " +
                // Set up the movie _id column as a foreign key to trailer table.
                " FOREIGN KEY (" + MovieContract.ReviewsEntry.COLUMN_REMOTE_MOVIE_ID + ") REFERENCES " +
                MovieContract.MovieEntry.TABLE_NAME + " (" + MovieContract.MovieEntry.COLUMN_MOVIE_ID + ") " +
                ");";


        final String SQL_CREATE_FAVORITES_TABLE = "CREATE TABLE " + MovieContract.FavoritesEntry.TABLE_NAME +
                "  ( " +

                MovieContract.FavoritesEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
                // the ID of the location entry associated with this weather data
                MovieContract.FavoritesEntry.COLUMN_MOVIE_ID + " INTEGER NOT NULL , " +
                MovieContract.FavoritesEntry.COLUMN_POSTER_URL + " TEXT NOT NULL, " +
                MovieContract.FavoritesEntry.COLUMN_TITLE + " TEXT NOT NULL, " +
                MovieContract.FavoritesEntry.COLUMN_RELEASE_DATE + " TEXT NOT NULL," +
                MovieContract.FavoritesEntry.COLUMN_VOTE_AVERAGE + " REAL NOT NULL, " +
                MovieContract.FavoritesEntry.COLUMN_OVERVIEW + " REAL NOT NULL, " +
                MovieContract.FavoritesEntry.COLUMN_BACKDROP + " TEXT NOT NULL, " +
                MovieContract.FavoritesEntry.COLUMN_QRY_CATEGORY + " TEXT NOT NULL, " +


                "UNIQUE (" + MovieContract.FavoritesEntry.COLUMN_TITLE + ") ON CONFLICT REPLACE);";


        // per movie id, it's created a UNIQUE constraint with REPLACE strategy
        sqLiteDatabase.execSQL(SQL_CREATE_MOVIE_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_TRAILER_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_REVIEW_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_FAVORITES_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        // Note that this only fires if you change the version number for your database.
        // It does NOT depend on the version number for your application.

        // If you want to update the schema without wiping data, commenting out the next 2 lines
        // should be your top priority before modifying this method.
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + MovieContract.MovieEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
