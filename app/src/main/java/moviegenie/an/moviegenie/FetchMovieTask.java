package moviegenie.an.moviegenie;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Vector;

import data.MovieContract;

/**
 * Created by Anu on 1/8/17.
 * This Async Task is to fetch the data from the MOVIE API
 */
public class FetchMovieTask extends AsyncTask<String,Void,Void> {
    // Return the poster paths to show it in the adapter

    private static Context mContext;
    public final static String LOG_TAG = FetchMovieTask.class.getSimpleName();
    public final static String POPULAR = "popular";
    public final static String TOPRATED = "top_rated";
    public final static String FAVORITES = "favorites";

    //constructor
    public FetchMovieTask(Context context) {
        Log.d(LOG_TAG,"inside FetchMovieTask Constructor");
        mContext = context;
    }


    /**
     * Take the String representing the complete Movie in JSON Format and
     * pull out the data we need to construct the Strings needed for the wireframes.
     *
     * Take the json string, convert to object
     *
     */
    private static String[] getMovieDataFromJson(String movieDataJsonStr,String queryParameter)
            throws JSONException {


        // These are the names of the JSON objects that need to be extracted.
        final String OWM_RESULTS = "results"; // list
        final String OWM_ID = "id";
        final String OWM_TITLE = "title";
        final String OWM_POSTERPATH = "poster_path";
        final String OWM_RELASEDATE = "release_date";
        final String OWM_OVERVIEW = "overview";
        final String OWM_VOTEAVERAGE = "vote_average";
        final String OWM_BACKDROPPATH = "backdrop_path";
        final String size = "w185";
        final String BASE_IMAGE_URL = "http://image.tmdb.org/t/p";

        try {
            // extract the Json object (all movies) from the jsonStr
            JSONObject movieJsonObject = new JSONObject(movieDataJsonStr);
            // Get the json array for the "results" data
            JSONArray resultsMovieArray = movieJsonObject.getJSONArray(OWM_RESULTS);
          //  Log.d(LOG_TAG, "results movie array = " + resultsMovieArray.length());

            // Create a vector of ContentValues object to insert the new Movie into the database
            // Get the Json Object
            Vector<ContentValues> contentValuesVector =
                    new Vector<ContentValues>(resultsMovieArray.length());


            // extract each object within the array "results"
            for (int i = 0; i < resultsMovieArray.length(); i++) {

                long movieId;
                String movieTitle;
                String posterPath;
                String posterPathUrl;
                String releaseDate;
                String overView;
                Double voteAverage;
                String backdropPath;
                String backdropPathUrl;

                JSONObject movieObject = resultsMovieArray.getJSONObject(i);
              //  Log.d(LOG_TAG,"movie object = " + movieObject.toString());

                // Get the json objects
                movieId = movieObject.getLong(OWM_ID);
                movieTitle = movieObject.getString(OWM_TITLE);
                posterPath = movieObject.getString(OWM_POSTERPATH);
                posterPathUrl = BASE_IMAGE_URL + "/" + size + posterPath;
                releaseDate = movieObject.getString(OWM_RELASEDATE);
                overView = movieObject.getString(OWM_OVERVIEW);
                voteAverage = movieObject.getDouble(OWM_VOTEAVERAGE);
                backdropPath = movieObject.getString(OWM_BACKDROPPATH);
                backdropPathUrl = BASE_IMAGE_URL + "/" + size + backdropPath;

                ContentValues movieContentValues = new ContentValues();
                // populate the movieContentValues
                movieContentValues.put(MovieContract.MovieEntry.COLUMN_MOVIE_ID, movieId);
                movieContentValues.put(MovieContract.MovieEntry.COLUMN_TITLE, movieTitle);
                movieContentValues.put(MovieContract.MovieEntry.COLUMN_POSTER_URL, posterPathUrl);
                movieContentValues.put(MovieContract.MovieEntry.COLUMN_RELEASE_DATE, releaseDate);
                movieContentValues.put(MovieContract.MovieEntry.COLUMN_OVERVIEW, overView);
                movieContentValues.put(MovieContract.MovieEntry.COLUMN_VOTE_AVERAGE, voteAverage);
                movieContentValues.put(MovieContract.MovieEntry.COLUMN_BACKDROP,backdropPathUrl);
                movieContentValues.put(MovieContract.MovieEntry.COLUMN_QRY_CATEGORY,queryParameter);


                // add this new value to vector array of contentValus
                contentValuesVector.add(movieContentValues);
            }
           // Log.d(LOG_TAG,"content vector size " +contentValuesVector.size());
            // add to database, using bulkinsert
            if (contentValuesVector.size() > 0) {
                // creating an array of size vectorArray
                ContentValues[] contentValuesArray = new ContentValues[contentValuesVector.size()];
                // returns an array containing all the elements in this vector in the correct order;
                // The runtimeType of the returned array is that of the specified array
                // toArray(T[] a);
                contentValuesVector.toArray(contentValuesArray);

                Uri uri = MovieContract.MovieEntry.CONTENT_URI;
               // Log.d(LOG_TAG," uri to bulk insert " + uri);
                mContext.getContentResolver().bulkInsert(MovieContract.MovieEntry.CONTENT_URI , contentValuesArray);
            }

        }catch (JSONException e) {
            Log.e(LOG_TAG ,e.getMessage(),e);
            e.printStackTrace();
        }

        return null;

    }

    // Extract the string json from the url, parse the string as json objects, add it
    // to database
    @Override
    protected Void doInBackground(String... params) {

        // if there is no "popular or toprated selected"
        if (params.length == 0) {
            return null;
        }
        Log.d(LOG_TAG," do InBackGround Processing - call the api");
        String queryCategory = params[0];
        Log.d(LOG_TAG,"query category = "+ queryCategory);

        // These two need to be declared outside the try/catch
        // so that they can be closed in the finally block.
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        // Will contain the raw JSON response as a string.
        String movieDataJsonStr = null;
        String type = "movie";

        try {
            //1. construct the url for the movie database.
            String MOVIEDB_BASE_URL = null;

            switch(queryCategory) {
                case POPULAR: {
                    MOVIEDB_BASE_URL = "https://api.themoviedb.org/3/movie/" + queryCategory + "?";
                    break;
                }
                case TOPRATED: {
                    MOVIEDB_BASE_URL = "https://api.themoviedb.org/3/movie/" + queryCategory + "?";
                    break;
                }

                default:
                    Log.d(LOG_TAG,"cannot do the api requests");
            }

            final String APPID_PARAM = "api_key";

            // 2. build a URI
            // https://api.themoviedb.org/3/movie/top_rated?APIKEY
            Uri builtUri = Uri.parse(MOVIEDB_BASE_URL)
                    .buildUpon()
                    .appendQueryParameter(APPID_PARAM, BuildConfig.MOVIE_DB_MAP_API_KEY)
                    .build();

            URL url = new URL(builtUri.toString());
          //  Log.d("LOG_TAG", "url built string = " + url);

            // 3. Create the request to MovieDB, and open the connection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // 4. Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            //5. read a line and append to String
            String line;
            while ((line = reader.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                return null;
            }
            //6. This is the final string
            movieDataJsonStr = buffer.toString();
         //   Log.d(LOG_TAG,"jsonString from url " + movieDataJsonStr);
        } catch (IOException e) {
            Log.e("LOG_TAG", "Error ", e);
            // If the code didn't successfully get the movieDB data, there's no point in attemping
            // to parse it.
            return null;
        } // IMPORTANT: make sure you make the url connection null
        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e("LOG_TAG", "Error closing stream", e);
                }
            }
        }//finally

        // 7. Extract the data from the String
        try {
            Log.d(LOG_TAG," call the json method to parse the string - calling from background processing");
            getMovieDataFromJson(movieDataJsonStr,queryCategory);
        } catch (JSONException e) {
            Log.e("LOG_TAG", e.getMessage(), e);
            e.printStackTrace();
        }

        return null;
    } // end of doInBackground processing

}
