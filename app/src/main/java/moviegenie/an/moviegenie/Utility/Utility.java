package moviegenie.an.moviegenie.Utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import moviegenie.an.moviegenie.R;

/**
 * Created by Anu on 1/4/17.
 */
public class Utility {
    // The sort category (popular or toprated) is set in sharedPreferences
    public static String getQueryCategory(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(context.getString(R.string.pref_sort_key),
                context.getString(R.string.pref_default_sortorder));

    }
}
