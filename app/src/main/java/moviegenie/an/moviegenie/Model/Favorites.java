package moviegenie.an.moviegenie.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Anu on 1/31/17.
 */

public class Favorites implements Parcelable {

    private final static String LOG_TAG = Movie.class.getSimpleName();

    long id;
    String posterUrl;
    String title;
    long remote_movieId;
    String release_date;
    Double voteAverage;
    String overview;
    String backdrop_path;


    public Favorites() {
        id = -1;
        posterUrl = null;
        title = null;
        remote_movieId = -1;
        release_date = null;
        voteAverage = 0.0;
        overview = null;
        backdrop_path = null;

    }


    //  title, release date, movie poster, vote average, and plot synopsis.

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getRemote_movieId() { return remote_movieId; }

    public void setRemote_movieId(Long id){ this.remote_movieId = remote_movieId;}

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getOverview() {
        return overview;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setVoteAverage(Double voteAverage) {
        this.voteAverage = voteAverage;
    }

    public Double getVoteAverage() {
        return voteAverage;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(posterUrl);
        parcel.writeString(title);
    }

    private Favorites(Parcel in){
        id = in.readLong();
        posterUrl = in.readString();
        title = in.readString();
    }

    // This is an Interface which must implement the 2 methods.
    // THis will create an instance of Parceleable class
    // you are creating a parcelable object called CREATOR which is of type Movie
    // TO create an instance you have a private constructor that takes in a parcel
    public final static Creator<Favorites> CREATOR = new Parcelable.ClassLoaderCreator<Favorites>(){

        @Override
        public Favorites createFromParcel(Parcel parcel) {
            return new Favorites(parcel);
        }

        @Override
        public Favorites[] newArray(int i) {
            return new Favorites[i];
        }

        @Override
        public Favorites createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return null;
        }
    };
}
