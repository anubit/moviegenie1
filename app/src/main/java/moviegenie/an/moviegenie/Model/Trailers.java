package moviegenie.an.moviegenie.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Anu on 2/1/17.
 */

public class Trailers implements Parcelable {

    long id;
    long remote_movie_id;
    String trailerTitle;
    String trailerKey;
    String columnType;

    public Trailers(){
        id = -1;
        remote_movie_id = -1;
        trailerKey = "UNKNOWN";
        trailerTitle = "UNKNOWN";
        columnType = "trailers";
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getRemote_movie_id() {
        return remote_movie_id;
    }

    public void setRemote_movie_id(long remote_movie_id) {
        this.remote_movie_id = remote_movie_id;
    }

    public String getTrailerTitle() {
        return trailerTitle;
    }

    public void setTrailerTitle(String trailerTitle) {
        this.trailerTitle = trailerTitle;
    }

    public String getTrailerKey() {
        return trailerKey;
    }

    public void setTrailerKey(String trailerKey) {
        this.trailerKey = trailerKey;
    }

    public String getColumnType() {
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeLong(remote_movie_id);
        parcel.writeString(trailerTitle);
        parcel.writeString(trailerKey);
        parcel.writeString(columnType);
    }

    private Trailers(Parcel in){
        id = in.readLong();
        remote_movie_id = in.readLong();
        trailerTitle = in.readString();
        trailerKey = in.readString();
        columnType= in.readString();
    }

    // This is an Interface which must implement the 2 methods.
    // THis will create an instance of Parceleable class
    // you are creating a parcelable object called CREATOR which is of type Movie
    // TO create an instance you have a private constructor that takes in a parcel
    public final static Creator<Trailers> CREATOR = new Parcelable.ClassLoaderCreator<Trailers>(){

        @Override
        public Trailers createFromParcel(Parcel parcel) {
            return new Trailers(parcel);
        }

        @Override
        public Trailers[] newArray(int i) {
            return new Trailers[i];
        }

        @Override
        public Trailers createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return null;
        }
    };

}

