package moviegenie.an.moviegenie.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Anu on 9/27/16.
 */
public class Movie implements Parcelable{
    long id;
    String posterUrl;
    String title;
    String release_date;
    Double voteAverage;
    String overview;
    String backdrop_path;
    String category;

    private final static String LOG_TAG = Movie.class.getSimpleName();
    

    public Movie() {
        id = 0;
        posterUrl = null;
        title = null;
        release_date = null;
        voteAverage = 0.0;
        overview = null;
        backdrop_path = null;
        category = null;

    }


    //  title, release date, movie poster, vote average, and plot synopsis.

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public String getBackdrop_path() { return backdrop_path;}

    public void setBackdrop_path(String backdrop_path) { this.backdrop_path = backdrop_path; }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getOverview() {
        return overview;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setVoteAverage(Double voteAverage) {
        this.voteAverage = voteAverage;
    }

    public Double getVoteAverage() {
        return voteAverage;
    }

    public String getCategory(){ return  category;}

    public void setCategory(String category) { this.category = category;}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(posterUrl);
        parcel.writeString(title);
        parcel.writeString(release_date);
        parcel.writeDouble(voteAverage);
        parcel.writeString(overview);
        parcel.writeString(backdrop_path);
    }

    private Movie(Parcel in){
        id = in.readLong();
        posterUrl = in.readString();
        title = in.readString();
        release_date = in.readString();
        voteAverage = in.readDouble();
        overview = in.readString();
        backdrop_path = in.readString();
    }

    // This is an Interface which must implement the 2 methods.
    // THis will create an instance of Parceleable class
    // you are creating a parcelable object called CREATOR which is of type Movie
    // TO create an instance you have a private constructor that takes in a parcel
    public final static Creator<Movie> CREATOR = new Parcelable.ClassLoaderCreator<Movie>(){

        @Override
        public Movie createFromParcel(Parcel parcel) {
            return new Movie(parcel);
        }

        @Override
        public Movie[] newArray(int i) {
            return new Movie[i];
        }

        @Override
        public Movie createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return null;
        }
    };
}
