package moviegenie.an.moviegenie.Model;

import android.os.Parcel;
import android.os.Parcelable;

import data.MovieContract;

/**
 * Created by Anu on 2/4/17.
 */

public class Reviews implements Parcelable {

    long id;
    long remote_movie_id;
    String reviewer;
    String desc;
    String columnType;

    public Reviews(){
        id = -1;
        remote_movie_id = -1;
        reviewer = "UNKNOWN";
        desc = "UNKNOWN";
        columnType = "reviews";
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getRemote_movie_id() {
        return remote_movie_id;
    }

    public void setRemote_movie_id(long remote_movie_id) {
        this.remote_movie_id = remote_movie_id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getReviewer() {
        return reviewer;
    }

    public void setReviewer(String reviewer) {
        this.reviewer = reviewer;
    }

    public String getColumnType() {
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeLong(remote_movie_id);
        parcel.writeString(desc);
        parcel.writeString(reviewer);
        parcel.writeString(columnType);
    }

    private Reviews(Parcel in){
        id = in.readLong();
        remote_movie_id = in.readLong();
        desc = in.readString();
        reviewer = in.readString();
        columnType= in.readString();
    }

    // This is an Interface which must implement the 2 methods.
    // THis will create an instance of Parceleable class
    // you are creating a parcelable object called CREATOR which is of type Movie
    // TO create an instance you have a private constructor that takes in a parcel
    public final static Creator<Reviews> CREATOR = new Parcelable.ClassLoaderCreator<Reviews>(){

        @Override
        public Reviews createFromParcel(Parcel parcel) {
            return new Reviews(parcel);
        }

        @Override
        public Reviews[] newArray(int i) {
            return new Reviews[i];
        }

        @Override
        public Reviews createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return null;
        }
    };

}

