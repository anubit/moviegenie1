package moviegenie.an.moviegenie.Activity.detail;


import android.content.AsyncQueryHandler;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import data.MovieContract;
import moviegenie.an.moviegenie.Activity.detail.Adapter.DetailAdapter;
import moviegenie.an.moviegenie.Activity.detail.Adapter.ReviewAdapter;
import moviegenie.an.moviegenie.Activity.main.MovieAdapter;
import moviegenie.an.moviegenie.FetchDetailsTask;
import moviegenie.an.moviegenie.FetchReviewsTask;
import moviegenie.an.moviegenie.Model.Favorites;
import moviegenie.an.moviegenie.Model.Movie;
import moviegenie.an.moviegenie.Model.Reviews;
import moviegenie.an.moviegenie.Model.Trailers;
import moviegenie.an.moviegenie.R;

/**
 * Created by Anu on 1/9/17.
 */

public class DetailFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {


    private static final String LOG_TAG = DetailFragment.class.getSimpleName();
    private static final String TRAILERKEY = "videos";
    private static final String REVIEWKEY = "reviews";
    private static final String FAVORITES = "favorites";

    public static final String DETAIL_SAVE_KEY= "details_save_key";
    public static final String TRAILER_SAVE_KEY = "trailers_save_key";
    public static final String REVIEWS_SAVE_KEY= "reviews_save_key";

    private static final int DETAIL_LOADER = 1;
    private static final int TRAILER_LOADER = 2;
    private static final int REVIEW_LOADER = 3;
    private static final int FAVORITES_LOADER = 4;
    private DetailAdapter detailAdapter;
    private ReviewAdapter reviewAdapter;
    private Uri favoritesUri;
    private String selection;
    private String[] selectionArgs;

    private  ContentValues contentValues;
    CoordinatorLayout mCoordinatorLayout;
    CollapsingToolbarLayout collapsingToolbar;

    public TextView titleView;
    public ImageView posterView;
    public TextView releaseDateView;
    public TextView voteAverageView;
    public TextView overViewView;
    public TextView toolbar_title;
    private QueryHandler mQueryHandler;

    // private ShareActionProvider mShareActionProvider;
    private Uri mUri;
    private long movieId = 0;

    // In this case the id needs to be fully qualified with a table name, since
    // the content provider joins the trailer & movies tables in the background
    // (both have an _id column)
    //  you can search the movie table to get all the details once user selects a movie id.
    protected static final String[] DETAIL_COLUMNS = {
            MovieContract.MovieEntry.TABLE_NAME + "." + MovieContract.MovieEntry._ID,
            MovieContract.MovieEntry.COLUMN_MOVIE_ID,
            MovieContract.MovieEntry.COLUMN_POSTER_URL,
            MovieContract.MovieEntry.COLUMN_TITLE,
            MovieContract.MovieEntry.COLUMN_RELEASE_DATE,
            MovieContract.MovieEntry.COLUMN_VOTE_AVERAGE,
            MovieContract.MovieEntry.COLUMN_OVERVIEW,
            MovieContract.MovieEntry.COLUMN_BACKDROP,
            MovieContract.MovieEntry.COLUMN_QRY_CATEGORY
    };

    // projection columns for Trailer database
    protected static final String[] TRAILER_COLUMNS = {
            MovieContract.TrailerEntry.TABLE_NAME + "." + MovieContract.TrailerEntry._ID,
            MovieContract.TrailerEntry.COLUMN_REMOTE_MOVIE_ID,
            MovieContract.TrailerEntry.COLUMN_TRAILER_TITLE,
            MovieContract.TrailerEntry.COLUMN_TRAILER_KEY,
            MovieContract.TrailerEntry.COLUMN_TYPE
    };

    // projection columns for Trailer database
    private static final String[] REVIEW_COLUMNS = {
            MovieContract.ReviewsEntry.TABLE_NAME + "." + MovieContract.ReviewsEntry._ID,
            MovieContract.ReviewsEntry.COLUMN_REMOTE_MOVIE_ID,
            MovieContract.ReviewsEntry.COLUMN_REVIEWER,
            MovieContract.ReviewsEntry.COLUMN_DESC,
            MovieContract.ReviewsEntry.COLUMN_TYPE,

    };

    private static final String[] FAVORITES_COLUMNS = {
            MovieContract.FavoritesEntry.TABLE_NAME + "." + MovieContract.FavoritesEntry._ID,
            MovieContract.FavoritesEntry.COLUMN_MOVIE_ID,
            MovieContract.FavoritesEntry.COLUMN_POSTER_URL,
            MovieContract.FavoritesEntry.COLUMN_TITLE,
            MovieContract.FavoritesEntry.COLUMN_RELEASE_DATE,
            MovieContract.FavoritesEntry.COLUMN_VOTE_AVERAGE,
            MovieContract.FavoritesEntry.COLUMN_OVERVIEW,
            MovieContract.FavoritesEntry.COLUMN_BACKDROP,
            MovieContract.FavoritesEntry.COLUMN_QRY_CATEGORY

    };


    // These indices are tied to DETAIL_COLUMNS.  If the above DETAIL_COLUMNS changes, these
    // must change.
    static final int COL_MOVIE_ID = 1;
    static final int COL_POSTER_URL = 2;
    static final int COL_TITLE = 3;
    static final int COL_RELEASE_DATE = 4;
    static final int COL_VOTE_AVERAGE = 5;
    static final int COL_OVERVIEW = 6;
    static final int COL_BACKDROP = 7;
    static final int COL_QRY_CATEGORY = 8;

    // Trailer columns indicies
    public static final int COL_TRAILER_ID = 0;
    public static final int COL_FOREIGN_MOVIE_ID = 1;
    public static final int COL_TRAILER_TITLE = 2;
    public static final int COL_TRAILER_KEY = 3;
    public static final int COL_TYPE = 4;

    // reviews col indicies
    public static final int COL_REVIEW_ID = 0;
    public static final int COL_REVIEWER = 2;
    public static final int COL_DESC = 3;

    // For tool bar
    private ImageView imgBackdrop;
    RecyclerView recyclerViewTrailer, recyclerViewReview;
    private Toolbar toolbar;
    private FloatingActionButton favoritesFAB;
    private Movie movieObj;
    private Trailers trailerObj;
    private Reviews reviewsObj;

    public DetailFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Uri path = getActivity().getIntent().getData();
       // Log.d(LOG_TAG, "path from movie " + path.toString());
        mUri = path;
        movieId = Long.parseLong(MovieContract.MovieEntry.getMovieIdFromUri(mUri));

        mQueryHandler = new QueryHandler(getContext());
        if (savedInstanceState == null) {
            startAsyncTask(movieId);
        }
        else {
            movieObj =savedInstanceState.getParcelable(DETAIL_SAVE_KEY);
            trailerObj = savedInstanceState.getParcelable(TRAILER_SAVE_KEY);
            reviewsObj = savedInstanceState.getParcelable(REVIEWS_SAVE_KEY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(LOG_TAG, "onCreate view");

        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);

        mCoordinatorLayout =
                (CoordinatorLayout) rootView.findViewById(R.id.coordinator_layout);
        toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        collapsingToolbar =
                (CollapsingToolbarLayout) rootView.findViewById(R.id.collapsing_toolbar);
        favoritesFAB = (FloatingActionButton) rootView.findViewById(R.id.favorites_fab);
        favoritesFAB.setOnClickListener(favoritesListener);


        imgBackdrop = (ImageView) rootView.findViewById(R.id.backdrop);

        titleView = (TextView) rootView.findViewById(R.id.tvTitle);
        releaseDateView = (TextView) rootView.findViewById(R.id.tvReleaseDate);
        posterView = (ImageView) rootView.findViewById(R.id.imgViewPoster);
        voteAverageView = (TextView) rootView.findViewById(R.id.tvVoteAverage);
        overViewView = (TextView) rootView.findViewById(R.id.tvPlot);

        recyclerViewTrailer = (RecyclerView) rootView.findViewById(R.id.detail_trailer_list);
        LinearLayoutManager llm = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerViewTrailer.setLayoutManager(llm);

        recyclerViewReview = (RecyclerView) rootView.findViewById(R.id.detail_reviewer_list);
        LinearLayoutManager llm2 = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerViewReview.setLayoutManager(llm2);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    /**
     * Async Task to fetch the trailers for a given movie id
     *
     * @param movieId
     */
    private void startAsyncTask(long movieId) {

        FetchDetailsTask fetchDetailsTask = new FetchDetailsTask(getContext());
        fetchDetailsTask.execute(String.valueOf(movieId), TRAILERKEY);

        FetchReviewsTask fetchReviewsTask = new FetchReviewsTask(getContext());
        fetchReviewsTask.execute(String.valueOf(movieId), REVIEWKEY);
    }

    /**
     * Initialize loaders here to fetch the data from the database
     * Make sure you call the Async Task for trailers before you call the loader
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        getLoaderManager().initLoader(DETAIL_LOADER, null, this);
        getLoaderManager().initLoader(TRAILER_LOADER, null, this);
        getLoaderManager().initLoader(REVIEW_LOADER, null, this);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(LOG_TAG, "on Resume of detail fragment");
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(DETAIL_SAVE_KEY,movieObj);
        outState.putParcelable(TRAILER_SAVE_KEY,trailerObj);
        outState.putParcelable(REVIEWS_SAVE_KEY,reviewsObj);
    }
    /**
     * create a cursor for the selected movie from the uri
     * uri is passed as a bundle
     */
    @Override
    public Loader<Cursor> onCreateLoader(int loaderId, Bundle bundle) {

        Loader<Cursor> cursorLoader = null;

        if (null != mUri) {

            switch (loaderId) {

                case DETAIL_LOADER: {
                    cursorLoader = new CursorLoader(getActivity(), mUri, DETAIL_COLUMNS, null, null,
                            null);
                    break;
                }

                case TRAILER_LOADER: {

                    Uri trailerUri = MovieContract.TrailerEntry.buildTrailerUri(movieId);

                    cursorLoader = new CursorLoader(getActivity(), trailerUri, TRAILER_COLUMNS, null, null,
                            null);

                    break;
                }
                case REVIEW_LOADER: {
                    Uri reviewUri = MovieContract.ReviewsEntry.buildReviewsUri(movieId);

                    cursorLoader = new CursorLoader(getActivity(), reviewUri, REVIEW_COLUMNS, null, null,
                            null);
                    break;
                }
                default:
                    Log.d(LOG_TAG, "cannot load any loader");

                    return null;
            }
        }
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

        if (!cursor.moveToFirst()) {
            return;
        }

        int loaderId = loader.getId();

        //load backdrop
        switch (loaderId) {
            case DETAIL_LOADER: {

                DatabaseUtils.dumpCursor(cursor);

                // Create a new Movie Object
                movieObj = new Movie();
                Long id = Long.parseLong(cursor.getString(DetailFragment.COL_MOVIE_ID));
                movieObj.setId(id);
                movieObj.setTitle(cursor.getString(DetailFragment.COL_TITLE));
                movieObj.setPosterUrl(cursor.getString(DetailFragment.COL_POSTER_URL));
                movieObj.setBackdrop_path(cursor.getString(DetailFragment.COL_BACKDROP));
                movieObj.setRelease_date(cursor.getString(DetailFragment.COL_RELEASE_DATE));
                double ave = Double.parseDouble(cursor.getString(DetailFragment.COL_VOTE_AVERAGE));
                movieObj.setVoteAverage(ave);
                movieObj.setOverview(cursor.getString(DetailFragment.COL_OVERVIEW));

                // From the uri get the backdrop path().
                String backdrop = cursor.getString(DetailFragment.COL_BACKDROP);

                Picasso.with(getContext())
                        .load(cursor.getString(DetailFragment.COL_BACKDROP))
                        .into(imgBackdrop);

                // loading the detail views

                collapsingToolbar.setTitle(cursor.getString(DetailFragment.COL_TITLE));
                titleView.setText(cursor.getString(DetailFragment.COL_TITLE));
                Picasso.with(getContext())
                        .load(cursor.getString(DetailFragment.COL_POSTER_URL))
                        .into(posterView);

                releaseDateView.setText
                        (cursor.getString(DetailFragment.COL_RELEASE_DATE));
                voteAverageView.setText
                        (cursor.getString(DetailFragment.COL_VOTE_AVERAGE));
                overViewView.setText
                        (cursor.getString(DetailFragment.COL_OVERVIEW));
                return;
            }
            case TRAILER_LOADER: {

                // create a trailer obj from the cursor
                Trailers trailerObj = new Trailers();
                long id = Long.parseLong(cursor.getString(DetailFragment.COL_TRAILER_ID));
                trailerObj.setId(id);
                Long movieId = Long.parseLong(cursor.getString(DetailFragment.COL_MOVIE_ID));
                trailerObj.setRemote_movie_id(movieId);
                trailerObj.setTrailerKey(cursor.getString(DetailFragment.COL_TRAILER_KEY));
                trailerObj.setTrailerTitle(cursor.getString(DetailFragment.COL_TRAILER_TITLE));
                trailerObj.setColumnType(cursor.getString(DetailFragment.COL_TYPE));

                //  DatabaseUtils.dumpCursor(cursor);
                detailAdapter = new DetailAdapter(getContext(), cursor);
                recyclerViewTrailer.setAdapter(detailAdapter);
                detailAdapter.swapCursor(cursor);

                return;
            }

            case REVIEW_LOADER: {

                // Creating this review object for later use in save Instance state
                //DatabaseUtils.dumpCursor(cursor);
                // create a trailer obj from the cursor
                Reviews reviewsObj = new Reviews();
                long id = Long.parseLong(cursor.getString(DetailFragment.COL_REVIEW_ID));
                reviewsObj.setId(id);
                Long movieId = Long.parseLong(cursor.getString(DetailFragment.COL_MOVIE_ID));
                reviewsObj.setRemote_movie_id(movieId);
                reviewsObj.setDesc(cursor.getString(DetailFragment.COL_DESC));
                reviewsObj.setReviewer(cursor.getString(DetailFragment.COL_REVIEWER));
                reviewsObj.setColumnType(cursor.getString(DetailFragment.COL_TYPE));


                reviewAdapter = new ReviewAdapter(getContext(), cursor);
                recyclerViewReview.setAdapter(reviewAdapter);
                reviewAdapter.swapCursor(cursor);
                return;

            }
            default: {
                Log.d(LOG_TAG, "not able to display cursor for any loaders");
            }
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    // favorites Listener
    private View.OnClickListener favoritesListener;

    {
        favoritesListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // persist the movie to favorites
                // create a favorites object and pass it to Async task to add to database
                updateFavorites(movieId);

            }
        };
    }


    public static int TOKEN_GROUP = 0;

    private void updateFavorites(long movieId) {

        favoritesUri = MovieContract.FavoritesEntry.buildFavoritesUri(movieId);


        contentValues = new ContentValues();
        contentValues.put(MovieContract.FavoritesEntry.COLUMN_MOVIE_ID, movieId);
        contentValues.put(MovieContract.FavoritesEntry.COLUMN_TITLE, movieObj.getTitle());
        contentValues.put(MovieContract.MovieEntry.COLUMN_POSTER_URL, movieObj.getPosterUrl());
        contentValues.put(MovieContract.MovieEntry.COLUMN_RELEASE_DATE, movieObj.getRelease_date());
        contentValues.put(MovieContract.MovieEntry.COLUMN_OVERVIEW, movieObj.getOverview());
        contentValues.put(MovieContract.MovieEntry.COLUMN_VOTE_AVERAGE, movieObj.getVoteAverage());
        contentValues.put(MovieContract.MovieEntry.COLUMN_BACKDROP,movieObj.getBackdrop_path());
        contentValues.put(MovieContract.MovieEntry.COLUMN_QRY_CATEGORY,FAVORITES);

        String selection =  MovieContract.FavoritesEntry.COLUMN_MOVIE_ID + " = ? ";
        String id = String.valueOf(movieId);
        String[] selectionArgs = new String[] {id};


        // Query for favorites with movie id
        // if it exists delete them
        // if id does not exist insert them;

        // 1. query for uri
        mQueryHandler.startQuery(TOKEN_GROUP,null,favoritesUri,FAVORITES_COLUMNS,selection,selectionArgs,null);

    }


    private class QueryHandler extends AsyncQueryHandler {

        public QueryHandler(Context context) {
            super(context.getContentResolver());
        }

        @Override
        protected void onInsertComplete(int token, Object cookie, Uri uri) {
            Toast.makeText(getContext(),"Added movies to Favorites", Toast.LENGTH_SHORT).show();
           // super.onInsertComplete(token, cookie, uri);

        }

        @Override
        protected void onQueryComplete(int token, Object cookie, Cursor cursor) {


            if ( (cursor == null) || (cursor.getCount() < 1) ) {
                mQueryHandler.startInsert(TOKEN_GROUP, null, favoritesUri, contentValues);
            }

            else { // if the movies exists in favorites delete from favorites
                Log.d(LOG_TAG, "movie exists - delete the id");

                while (cursor.moveToNext()) {
                    mQueryHandler.startDelete(TOKEN_GROUP,null,favoritesUri,selection,selectionArgs);
                }
            }
        }

        @Override
        protected void onDeleteComplete(int token, Object cookie, int result) {
           // Toast.makeText(getContext(),"Deleting from favorites", Toast.LENGTH_SHORT).show();
            super.onDeleteComplete(token, cookie, result);
        }
    }
}
