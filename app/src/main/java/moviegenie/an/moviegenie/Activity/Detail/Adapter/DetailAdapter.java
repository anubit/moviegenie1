package moviegenie.an.moviegenie.Activity.detail.Adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import moviegenie.an.moviegenie.Activity.detail.DetailFragment;
import moviegenie.an.moviegenie.R;

import static android.content.Intent.ACTION_VIEW;

/**
 * Created by Anu on 9/28/16.
 */

public class DetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final static String LOG_TAG = DetailAdapter.class.getSimpleName();

    //2. get the view count
    private static final int VIEW_TYPE_COUNT = 1;
    private Context mContext;
    private Cursor mCursor;


    public DetailAdapter(Context context, Cursor c) {
        this .mContext = context;
        this.mCursor = c;
    }

    /**
     * cache of the children views for listview trailer item
     */
    public static class ViewHolder_Trailer extends RecyclerView.ViewHolder {

        public final TextView titleView;
        public final Button imgBtnPlay;

        public ViewHolder_Trailer(View view) {
            super(view);
            titleView = (TextView) view.findViewById(R.id.movie_trailer_text_title);
            imgBtnPlay = (Button) view.findViewById(R.id.movie_trailer_play_button);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;

        int layoutId = -1;
        // View view = LayoutInflater.from(mContext).inflate(R.layout.movie_trailer_review, parent, false);
        // Create a ViewHolder object to store references
        // allows to find the view without going thru the tree(findViewById)
        if (parent instanceof RecyclerView) {

            layoutId = R.layout.movie_trailer_item;
            View view = LayoutInflater.from(mContext).inflate(layoutId, parent, false);
            viewHolder = new DetailAdapter.ViewHolder_Trailer(view);
            Log.d(LOG_TAG, "view holder =" + viewHolder.toString());
            return viewHolder;
        }
        else {
            throw new RuntimeException("Not bound to RecyclerView");
        }

    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        mCursor.moveToPosition(position);

        //2. get the type of the view
        int viewType = getItemViewType(mCursor.getPosition());
        Log.d(LOG_TAG, "view type = " + viewType);

        DetailAdapter.ViewHolder_Trailer holder_trailer = (DetailAdapter.ViewHolder_Trailer) holder;
        holder_trailer.titleView.setText(mCursor.getString(DetailFragment.COL_TRAILER_TITLE));
        final String trailerkey = mCursor.getString(DetailFragment.COL_TRAILER_KEY);
        holder_trailer.imgBtnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent youtubeIntent = new Intent(ACTION_VIEW,
                        Uri.parse("http://www.youtube.com/watch?v=" + trailerkey));
                v.getContext().startActivity(youtubeIntent);
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        int type = 0;
        return type;
    }

    @Override
    public int getItemCount() {
        if (mCursor == null || mCursor.getCount() == 0) {
            return 0;
        } else {
            return mCursor.getCount();
        }
    }

    public Cursor getItem(int position) {
        if (mCursor != null) {
            mCursor.moveToPosition(position);
            return mCursor;
        } else return null;
    }

    public void swapCursor(Cursor newCursor) {

        Log.d(LOG_TAG, "swap cursor method");

        if (newCursor != null) {
            mCursor = newCursor;
            // notify the observers about the new cursor
            notifyDataSetChanged();
        } else {
            Log.d(LOG_TAG, "new cursor is  null");
            notifyItemRangeRemoved(0, getItemCount());
            mCursor = null;
        }
    }
}

