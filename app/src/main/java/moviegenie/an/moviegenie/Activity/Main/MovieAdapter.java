package moviegenie.an.moviegenie.Activity.main;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import moviegenie.an.moviegenie.R;


/**
 * Created by Anu on 9/28/16.
 */

  public class MovieAdapter extends CursorAdapter {

    private final static String LOG_TAG = MovieAdapter.class.getSimpleName();
    private Context mContext;

    // 3rd parameter for auto query
    public MovieAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
       // Log.d(LOG_TAG,"cursor count " + c.getCount());
        this.mContext = context;
    }

    /**
     * Cache of the children views for a overview list item
     * Create a ViewHolder object to store references
     * allows to find the view without going thru the tree(findViewById)
     * via tags you can store the information about the view in a view itself
     */
    public static class ViewHolder {
        public final ImageView posterView;

        public ViewHolder(View view) {
            posterView = (ImageView) view.findViewById(R.id.imgView);
        }
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_gridview_item, parent, false);
        ViewHolder VH = new ViewHolder(view);
        view.setTag(VH);

        return view;
    }


    // fill in the view with the contents of the cursor
    @Override
    public void bindView(View convertView, Context context, Cursor cursor) {
        //1. get the tag of the view
            ViewHolder viewHolder = (ViewHolder) convertView.getTag();
            String imageUrl = cursor.getString(MovieFragment.COL_POSTER_URL);

            Picasso.with(context)
                    .load(imageUrl)
                    .placeholder(R.drawable.user_placeholder)
                    .error(R.drawable.user_placeholder_error)
                    .into(viewHolder.posterView);
    }
}

