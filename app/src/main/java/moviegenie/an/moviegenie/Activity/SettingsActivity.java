package moviegenie.an.moviegenie.Activity;

/**
 * Created by Anu on 10/3/16.
 */


import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import data.MovieContract;
import moviegenie.an.moviegenie.Model.Favorites;
import moviegenie.an.moviegenie.R;

/**
 * A {@link PreferenceActivity} that presents a set of application settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends AppCompatActivity {

    public static String LOG_TAG = "SettingsActivity.java";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction()
                .add(android.R.id.content, new PrefFragment()).commit();
    }

    public static class PrefFragment extends PreferenceFragment
            implements Preference.OnPreferenceChangeListener {

        final private String CUSTOM_LIST= "custom_list";

        //Load the preferences from an XML resource
        // Add 'general' preferences, defined in the XML file
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            addPreferencesFromResource(R.xml.pref_general);

            Log.d(LOG_TAG, " favorites entry count =" + MovieContract.FavoritesEntry._COUNT);

           /* PreferenceCategory preferenceCategory = (PreferenceCategory) findPreference("CUSTOM_FRAG");
            final ListPreference lp = setListPreferenceData((ListPreference) findPreference(CUSTOM_LIST));*/

            Log.d(LOG_TAG,"onCreate of preference fragment");

            // For all preferences, attach an OnPreferenceChangeListener so the UI summary can be
            // updated when the preference changes.

            // Attach the key of the sort order to bindPreferenceSummary
            bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_sort_key)));
        }

        /*
        protected ListPreference setListPreferenceData(ListPreference lp){
            CharSequence[] entries = {"@string/pref_sort_label_popular",
                    "@string/pref_sort_label_high_rated",
                    "@string/pref_sort_label_favorites"
            };
            CharSequence[] entryValues = {"@string/pref_sort_values_popular",
                    "@string/pref_sort_values_high_rated",
                    "@string/pref_sort_values_favorites"
            };
            lp.setEntries(entries);

        }*/
        /**
         * Attaches a listener so the summary is always updated with the preference value.
         * Also fires the listener once, to initialize the summary (so it shows up before the value
         * is changed.)
         */
        private void bindPreferenceSummaryToValue(Preference preference) {
            // Set the listener to watch for value changes.
            preference.setOnPreferenceChangeListener(this);

            // Trigger the listener immediately with the preference's
            // current value.
            onPreferenceChange(preference,
                    PreferenceManager
                            .getDefaultSharedPreferences(preference.getContext())
                            .getString(preference.getKey(), ""));
        }


        // For that key(sort order) set the value
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {

            String stringValue = value.toString();
            Log.d(LOG_TAG,"preference value is changed and the new value = " + value);
            if (preference instanceof ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list (since they have separate labels/values).
                ListPreference listPreference = (ListPreference) preference;
                int prefIndex = listPreference.findIndexOfValue(stringValue);
               // Log.d(LOG_TAG,"pref index = " + prefIndex);
                if (prefIndex >= 0) {
                    Log.d(LOG_TAG,"listPreference.getEntries = " + listPreference.getEntries());
                    preference.setSummary(listPreference.getEntries()[prefIndex]);
                }
            }  else {
                // For other preferences, set the summary to the value's simple string representation.
                preference.setSummary(stringValue);
            }
            return true;
        }
    }
}