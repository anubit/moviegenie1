package moviegenie.an.moviegenie.Activity.detail.Adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import moviegenie.an.moviegenie.Activity.detail.DetailFragment;
import moviegenie.an.moviegenie.Activity.detail.ExpandableTextView;
import moviegenie.an.moviegenie.R;


public class ReviewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final static String LOG_TAG = ReviewAdapter.class.getSimpleName();


    //2. get the view count
    private static final int VIEW_TYPE_COUNT = 1;
    private Context mContext;
    private Cursor mCursor;


    public ReviewAdapter(Context context, Cursor c) {
        this.mCursor = c;
        this.mContext = context;
    }


    public static class ViewHolder_Reviews extends RecyclerView.ViewHolder{

        public final TextView author;
      //  public final TextView desc;
        public ExpandableTextView desc;

        public ViewHolder_Reviews(View view) {
            super(view);
            author = (TextView) view.findViewById(R.id.movie_review_author);

            desc = (ExpandableTextView) view.findViewById(R.id.movie_review_desc);



        }
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;

        int layoutId = -1;
        // View view = LayoutInflater.from(mContext).inflate(R.layout.movie_trailer_review, parent, false);
        // Create a ViewHolder object to store references
        // allows to find the view without going thru the tree(findViewById)
        if (parent instanceof RecyclerView) {

            layoutId = R.layout.movie_review_item;
            View view = LayoutInflater.from(mContext).inflate(layoutId, parent, false);
            viewHolder = new ViewHolder_Reviews(view);

            return viewHolder;
        }
             else {
            throw new RuntimeException("Not bound to RecyclerView");
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        mCursor.moveToPosition(position);
        ViewHolder_Reviews holder_reviews = (ViewHolder_Reviews) holder;

        holder_reviews.author.setText(mCursor.getString(DetailFragment.COL_REVIEWER));
        holder_reviews.desc.setText(mCursor.getString(DetailFragment.COL_DESC));

    }

    @Override
    public int getItemViewType(int position) {
      return 0; // only one type
    }

    @Override
    public int getItemCount() {
        if (mCursor == null || mCursor.getCount() == 0) {
            return 0;
        }
        else {
            return mCursor.getCount();
        }
    }

    public Cursor getItem(int position){
        if (mCursor != null){
            mCursor.moveToPosition(position);
            return mCursor;
        }
        else return null;
    }
    public void swapCursor(Cursor newCursor) {

        if (newCursor != null) {
            mCursor = newCursor;
            // notify the observers about the new cursor
            notifyDataSetChanged();
        } else {
            notifyItemRangeRemoved(0, getItemCount());
            mCursor = null;
        }
    }
}

