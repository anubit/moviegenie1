package moviegenie.an.moviegenie.Activity.detail;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import moviegenie.an.moviegenie.R;

/**
 * Created by Anu on 10/4/16.
 */
public class DetailActivity extends AppCompatActivity {

    private static String LOG_TAG = DetailActivity.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Log.d(LOG_TAG, "inside onCreate of detail");

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace((R.id.detail_activity_container), new DetailFragment())
                    .commit();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

}
