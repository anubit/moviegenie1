package moviegenie.an.moviegenie.Activity.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import moviegenie.an.moviegenie.Activity.SettingsActivity;
import moviegenie.an.moviegenie.R;

/**
 * Main Activity
 * Adding the movie fragment to main activity
 */
public class MainActivity extends AppCompatActivity {

    private  final String LOG_TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Log.d(LOG_TAG,"onCreate - MainActivity");
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Fragment fragment = getSupportFragmentManager().findFragmentByTag("FRAGMENT");

        // If the saved state bundle is not null, the system already has a fragment
        // Load that fragment and you shouldn't go adding another one
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.main_container,new MovieFragment())
                    .commit();
        }
        
    }

    @Override
    protected void onResume() {
        Log.d(LOG_TAG,"Onresume of main Activity");
        super.onResume();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
        }
            return true;
    }


}
