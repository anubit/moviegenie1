package moviegenie.an.moviegenie.Activity.main;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import data.MovieContract;
import moviegenie.an.moviegenie.Activity.detail.DetailActivity;
import moviegenie.an.moviegenie.FetchMovieTask;
import moviegenie.an.moviegenie.Model.Favorites;
import moviegenie.an.moviegenie.R;
import moviegenie.an.moviegenie.Utility.Utility;

/**
 * fetching the movie and displaying it as a gridview layout
 * This fragment does the fetching the movie data from movie API via Loader Api
 */
public class MovieFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private final static String LOG_TAG = MovieFragment.class.getSimpleName();
    private final static String FAVORITES = "favorites";
    // initialize the loader id
    public static final int MOVIE_LOADER = 0;
    public static final int FAVORITE_LOADER = 1;
    private static final String DEFAULT_CRITERIA = "popular";


    private GridView gridView;
    private MovieAdapter movieAdapter;
    private int mPostion = GridView.INVALID_POSITION;
    private String mQuery;
    private Cursor mCursor;
    private Uri mUri;
    private TextView tvFavorites;


    int count = 0;
    // This is used in saveInstanceState when the screen rotates
    private static final String SELECTED_KEY = "selected_position";

    // These are the projection columns to retrieve from the Loader
    // Loader will use AsyncTask cursor Loader
    // These columns are used in creating a new cursor loader
    private static final String[] MOVIE_COLUMNS = {
            // In this case the id needs to be fully qualified with a table name, since
            // the content provider joins the favorites,trailers & movies tables in the background
            // (both have an _id column)
            //  you can search the movie table to get all the details once user selects a movie id.


            MovieContract.MovieEntry.TABLE_NAME + "." + MovieContract.MovieEntry._ID,
            MovieContract.MovieEntry.COLUMN_MOVIE_ID,
            MovieContract.MovieEntry.COLUMN_POSTER_URL,


    };

    private static final String[] FAVORITE_COLUMNS = {

            MovieContract.FavoritesEntry.TABLE_NAME + "." + MovieContract.FavoritesEntry._ID,
            MovieContract.FavoritesEntry.COLUMN_MOVIE_ID,
            MovieContract.MovieEntry.COLUMN_POSTER_URL,


    };

    // These indices are tied to FORECAST_COLUMNS.  If FORECAST_COLUMNS changes, these
    // must change.
    protected static final int COL_MOVIE_ID = 1;
    protected static final int COL_POSTER_URL = 2;


    public MovieFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // whatever is stored in settings
        mQuery = Utility.getQueryCategory(getActivity());

        // default - show movie
     //   if (savedInstanceState == null) {
            //Log.d(LOG_TAG, "movie fragment is null - call the Async Task to fetch the data from the API");
            updateMovie(mQuery);
     //   }

       // Log.d(LOG_TAG, "get the query order mquery in onCreate of movie fragment " + mQuery);
        // Add this line in order for this fragment to handle menu events
        // setHasOptionsMenu(true);

    }


    //2. initialize the loader in onActivityCreated for fragments
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.d(LOG_TAG, "on activity created -Movie Fragment- initialize the loader ");
        if (mQuery.equals("favorites")) {// initialize the loader to load favorites movies
            getLoaderManager().initLoader(FAVORITE_LOADER, null, this);
        } else {
            getLoaderManager().initLoader(MOVIE_LOADER, null, this);
        }
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onResume() {
        super.onResume();
        String userChangedSettings = Utility.getQueryCategory(getActivity());

        if (userChangedSettings != null && !userChangedSettings.equals(mQuery)) {

            mQuery = userChangedSettings; // changed to userChangedSettings
            Log.d(LOG_TAG, "after the new query criteria: " + mQuery);
            onQueryCriteria(mQuery);
        }

    }

    // when the user rotates the phone, the current selected item needs to be saved.
    // when no item is selected, mPosition will be set to GridView.INVALID_POSITION,
    // check before storing
    @Override
    public void onSaveInstanceState(Bundle outState) {

        if (mPostion != GridView.INVALID_POSITION) {
            outState.putInt(SELECTED_KEY, mPostion);
        }
        super.onSaveInstanceState(outState);
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        gridView = (GridView) rootView.findViewById(R.id.gridViewLayout);
        gridView.setOnItemClickListener((AdapterView.OnItemClickListener) gridviewListener);

        // If savedInstance is not null that means we have saved in the bundle.
        // when the device is rotated by the user, you should show the same state as before rotation
        if (savedInstanceState != null && savedInstanceState.containsKey(SELECTED_KEY)) {
            // perform the swap out cursor in onLoadFinished.
            // The grid view may not have been populated
            mPostion = savedInstanceState.getInt(SELECTED_KEY);
        }
        return rootView;
    }

    private AdapterView.OnItemClickListener gridviewListener =
            new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    // 1. get the item at the current position via cursor
                    //Cursor Adapter returns a cursor at the current position via getItem or returns null
                    Log.d(LOG_TAG, "inside gridview - onClick");
                    Cursor cursor = (Cursor) adapterView.getItemAtPosition(position);
                    if (cursor != null) {

                        long movieId = cursor.getLong(COL_MOVIE_ID);
                        Log.d(LOG_TAG, " build the uri using the movie id - call the buildMovieUri");
                        Log.d(LOG_TAG, "passing this uri to detail fragment " +
                                MovieContract.MovieEntry.buildMovieUri(movieId).toString());

                        // calling the DetailActivity class with the movie id uri
                        Intent intent = new Intent(getActivity(), DetailActivity.class)
                                .setData(MovieContract.MovieEntry.buildMovieUri(movieId));
                        startActivity(intent);
                    }
                    // the position that is selected
                    mPostion = position;
                }
            };

    /**
     * This is called
     */
    public void onQueryCriteria(String query) {
        updateMovie(query);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }


    // // fetch the category from sharedPreferences, default is popularity
    // call the AsyncTask Loader
    private void updateMovie(String queryCriteria) {
        // Async Task is to fetch the data from the MOVIE API.

        // Fetch Favorites from content provier
        if (queryCriteria.equals(FAVORITES)) {
            getLoaderManager().initLoader(FAVORITE_LOADER, null, this);
        } else { // fetch popular/top_rated movies from Api
            Log.d(LOG_TAG, "call fetch movie async task with query criteria = " + queryCriteria);
            startAsyncTask(queryCriteria);
        }
    }

    private void startAsyncTask(String queryCriteria){
        FetchMovieTask fetchMovieAsyncTask = new FetchMovieTask(getContext());
        fetchMovieAsyncTask.execute(queryCriteria);
        getLoaderManager().restartLoader(MOVIE_LOADER, null, this);
    }
    // create the URI to pass it to content provider
    // content provider will pass it to Loader to fetch the data via API reeusts
    @Override
    public Loader onCreateLoader(int loaderId, Bundle args) {

        switch (loaderId) {

            case FAVORITE_LOADER: {
                     // build favorites uri
                mUri = MovieContract.FavoritesEntry.
                        buildFavoritesUriWithQueryCriteria(mQuery);
                return new CursorLoader(getActivity(),
                        mUri,
                        FAVORITE_COLUMNS, // projected columns
                        null, // selected args
                        null, //having
                        null); // sortby

            }

            case MOVIE_LOADER: {
                mUri = MovieContract.MovieEntry.
                        buildMovieUriWithQueryCriteria(mQuery);

                // pass the uri to cursor loader: CursorLoader(context,uri,projection columns)
                // 3. create a new cursorloader
                // Cursor Loader extends AsyncTask Loader which uses an AsyncTask to fetch the data
                // from the database after the FetchMovieTask fetches the data from the movie API.

                return new CursorLoader(getActivity(),
                        mUri,
                        MOVIE_COLUMNS, // projected columns
                        null, // selected args
                        null, //having
                        null); // sortby
            }
            default:
                Log.d(LOG_TAG, "loader is null");
                break;
        }
        return null;
    }

    // Notify the adapter about the new cursor data via swapCursor
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

        movieAdapter = new MovieAdapter(getActivity(), cursor, 0);

        gridView.setAdapter(movieAdapter);
        movieAdapter.swapCursor(cursor);

        // This is set to make sure if the last item is selected/highlighted in potrait mode, on
        // screen rotation, to preserve the selected list item and immediately scrolled to that
        // highlighted item which is saved in savedInstanceState.
        // If not set, the highlighted item will be at the bottom and you need
        // to scroll to position to see the highlighted item.
        if (mPostion != GridView.INVALID_POSITION) {
            gridView.smoothScrollToPosition(mPostion);
        }

    }

    @Override
    public void onLoaderReset(Loader loader) {
       // movieAdapter.swapCursor(null);
    }

}
