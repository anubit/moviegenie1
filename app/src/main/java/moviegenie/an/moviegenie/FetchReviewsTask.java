package moviegenie.an.moviegenie;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Vector;

import data.MovieContract;

/**
 * Created by Anu on 1/20/17.
 * This Async Task is to fetch the trailer/reviews keys from the MOVIE API
 */
public class FetchReviewsTask extends AsyncTask<String,Void,Void> {
    // Return the poster paths to show it in the adapter

    private static Context mContext;
    public final static String LOG_TAG = FetchReviewsTask.class.getSimpleName();

    //constructor
    public FetchReviewsTask(Context context) {
        mContext = context;
    }


    /**
     * Take the String representing the complete Movie in JSON Format and
     * pull out the data we need to construct the Strings needed for the wireframes.
     *
     * Take the json string, convert to object
     *
     */
    private static String[] getReviewDataFromJson(String reviewsDataJsonStr,String type)
            throws JSONException {


        // These are the names of the JSON objects that need to be extracted.
        final String OWM_ID = "id";
        final String OWM_RESULTS = "results"; // list
        final String OWM_CONTENT = "content";
        final String OWN_REVIEWER = "author";

        try {
            // extract the Json object (all movies) from the jsonStr
            JSONObject reviewJsonObject = new JSONObject(reviewsDataJsonStr);
            // Get the json array for the "results" data
            JSONArray resultsReviewsArray = reviewJsonObject.getJSONArray(OWM_RESULTS);
           // Log.d(LOG_TAG, "results reviews array = " + resultsReviewsArray.length());

            // Create a vector of ContentValues object to insert the new review into the database
            // Get the Json Object
            Vector<ContentValues> contentValuesVector =
                    new Vector<ContentValues>(resultsReviewsArray.length());


            // extract each object within the array "results"
            for (int i = 0; i < resultsReviewsArray.length(); i++) {

                String content;
                String movieId;
                String reviewer;

                JSONObject reviewsObject = resultsReviewsArray.getJSONObject(i);
                // Log.d(LOG_TAG,"trailer object = " + trailerObject.toString());

                // Get the json objects
                content = reviewsObject.getString(OWM_CONTENT);
             //   Log.d(LOG_TAG," the content =" + content);
                movieId = reviewJsonObject.getString(OWM_ID);
                reviewer = reviewsObject.getString(OWN_REVIEWER);
                ContentValues reviewsContentValues = new ContentValues();
                // populate the movieContentValues
                reviewsContentValues.put(MovieContract.ReviewsEntry.COLUMN_DESC, content);
                reviewsContentValues.put(MovieContract.ReviewsEntry.COLUMN_REMOTE_MOVIE_ID,movieId);
                reviewsContentValues.put(MovieContract.ReviewsEntry.COLUMN_REVIEWER,reviewer);
                reviewsContentValues.put(MovieContract.ReviewsEntry.COLUMN_TYPE,type);

                // add this new value to vector array of contentValus
                contentValuesVector.add(reviewsContentValues);
            }
            Log.d(LOG_TAG,"content vector size " +contentValuesVector.size());
            // add to database, using bulkinsert
            if (contentValuesVector.size() > 0) {
                // creating an array of size vectorArray
                ContentValues[] contentValuesArray = new ContentValues[contentValuesVector.size()];
                // returns an array containing all the elements in this vector in the correct order;
                // The runtimeType of the returned array is that of the specified array
                // toArray(T[] a);
                contentValuesVector.toArray(contentValuesArray);

                Uri uri = MovieContract.ReviewsEntry.CONTENT_URI;
              //  Log.d(LOG_TAG," review uri to bulk insert " + uri);
                mContext.getContentResolver().bulkInsert(MovieContract.ReviewsEntry.CONTENT_URI , contentValuesArray);
            }

        }catch (JSONException e) {
            Log.e(LOG_TAG ,e.getMessage(),e);
            e.printStackTrace();
        }

        return null;

    }


    // Extract the string json from the url, parse the string as json objects, add it
    // to database
    @Override
    protected Void doInBackground(String... params) {

        // TODO: FAVORITES IS EMPTY initially? How do deal with that
        // if there is no "popular or toprated selected"
        if (params.length == 0) {
            return null;
        }

        String movieId = params[0];
        String type  = params[1]; // video or reviews

        // These two need to be declared outside the try/catch
        // so that they can be closed in the finally block.
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        // Will contain the raw JSON response as a string.
        String detailsDataJsonStr = null;

        try {
            //1. construct the url for the movie database.
            String BASE_URL = "https://api.themoviedb.org/3/movie/"+movieId+"/";
            String to_PARSEURL = null;
            switch(type) {
                // https://api.themoviedb.org/3/movie/328111/reviews?api_key=""
                case "reviews":
                    to_PARSEURL = BASE_URL+type+"?=";
                    break;

                default:
                    Log.d(LOG_TAG,"cannot do the api requests");
            }

            final String APPID_PARAM = "api_key";

            // 2. build a URI
            // https://api.themoviedb.org/3/movie/top_rated?APIKEY
            Uri builtUri = Uri.parse(to_PARSEURL)
                    .buildUpon()
                    .appendQueryParameter(APPID_PARAM, BuildConfig.MOVIE_DB_MAP_API_KEY)
                    .build();

            URL url = new URL(builtUri.toString());
            Log.d("LOG_TAG", "url built string = " + url);

            // 3. Create the request to MovieDB, and open the connection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // 4. Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            //5. read a line and append to String
            String line;
            while ((line = reader.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                return null;
            }
            //6. This is the final string
            detailsDataJsonStr = buffer.toString();
          //  Log.d(LOG_TAG,"jsonString from url " + detailsDataJsonStr);
        } catch (IOException e) {
            Log.e("LOG_TAG", "Error ", e);
            // If the code didn't successfully get the movieDB data, there's no point in attemping
            // to parse it.
            return null;
        } // IMPORTANT: make sure you make the url connection null
        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e("LOG_TAG", "Error closing stream", e);
                }
            }
        }//finally

        // 7. Extract the data from the String
        try {
          //  Log.d(LOG_TAG,"create review data");
            getReviewDataFromJson(detailsDataJsonStr, type);

        } catch (JSONException e) {
            Log.e("LOG_TAG", e.getMessage(), e);
            e.printStackTrace();
        }

        return null;
    } // end of doInBackground processing

}

