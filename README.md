# Movie Genie 1.0  #

This Android app allows you to view trailers and reviews of popular/ top rated movies from "The Movie Database". It also allows you to store your favorite movies.

* Implemented Movie Database using SQLite library 
* Integrated Content Provider and Loader with SQLite
* Displayed movie images using Picasso


####  Summary of set up ####
* Apply for an Api key from this website " https://www.themoviedb.org/faq/api".
* Plug your api key in gradle.properties: myMovieDbApiKey="YOUR API KEY"
#### Dependencies####
 compile 'com.squareup.picasso:picasso:2.5.2'
#### Supported Version
Android 5.0 and above

![moviegenie_gif1.gif](https://bitbucket.org/repo/4exrX7/images/1021186135-moviegenie_gif1.gif)